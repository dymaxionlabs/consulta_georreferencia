import requests
import urllib.parse

ARCHIVO_DATOS = "espacios_comunitarios.csv"
ARCHIVO_SALIDA = "espacios_comunitarios_coordenadas.csv"

API_BASE_URL = "https://apis.datos.gob.ar/georef/api/"

def get_similar(endpoint, nombre, **kwargs):
    kwargs["nombre"] = nombre
    url = "{}{}?{}".format(API_BASE_URL, endpoint, urllib.parse.urlencode(kwargs))
    return requests.get(url).json()[endpoint]

with open(ARCHIVO_DATOS, 'r') as fp:
    lineas = fp.readlines()

encabezado = lineas[0].strip()
encabezado += ",Latitud,Longitud\n"
with open(ARCHIVO_SALIDA, 'w') as fp2:
    fp2.write(encabezado)    

for linea in lineas[1:]:
    campos = linea.split(',')
    info = get_similar("localidades", campos[3])
    if len(info) > 0:
        linea = linea.strip()
        linea += ",{},{}\n".format(info[0]['centroide']['lat'],info[0]['centroide']['lon'])
    print(linea)
    with open(ARCHIVO_SALIDA, 'a') as fp2:
        fp2.writelines(linea)
