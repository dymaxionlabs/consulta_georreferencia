Uso de la API para georreferencia de Espacios Comunitarios del plan ENIA



En Dymaxion Labs creamos un programa escrito en lenguaje python que, a partir de un archivo con las localidades de cada espacio comunitario, genera una consulta a la API de Georreferenciación del Ministerio de Modernización. El objetivo de esta consulta es obtener las coordenadas(latitud y longitud) del Espacio Comunitario en cuestión.

Luego, a partir del conjunto de consultas de todos los espacios comunitarios se crea un segundo archivo que contiene la misma información que el archivo original, más las coordenadas obtenidas gracias a la API.

Dichas consultas fueron verificadas utilizando Google Maps.
